﻿public interface IDailyLeaderVoteView
{
    void ShowUsers(User[] users);
    void ShowConfirmationOfVoteFor(string userId);
    void ShowVotes(Vote[] votes);
    void ShowWinner(User user);
    void ShowLeaderboard(User[] users);
}