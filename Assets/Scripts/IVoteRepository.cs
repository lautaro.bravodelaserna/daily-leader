﻿using System;

public interface IVoteRepository
{
    void Save(Vote vote);
    Vote[] GetVotes();
    bool Exists(Func<Vote,bool> predicate);
}