﻿
public interface IUserRepository
{
    User GetUserById(string userId);
    User[] GetUsers();
    void Save(User user);
}