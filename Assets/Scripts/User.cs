﻿public class User
{
    public string Id;
    public int Score;
    public int Wins;

    public User(string id, int score = 0, int wins = 0)
    {
        Id = id;
        Score = score;
        Wins = wins;
    }
    
    public void IncreaseWins()
    {
        Wins++;
    }

    public User IncreaseScore()
    {
        Score += 100;
        return this;
    }

    public bool Is(string userId)
    {
        return Id == userId;
    }

    public override bool Equals(object obj)
    {
        var user = (User) obj;
        if (user == null)
            return false;
        return user.Id == Id;
    }
}