﻿using JetBrains.Annotations;

public class VoteRegister
{
    private readonly IVoteRepository _voteRepository;
    private readonly RetrieveUsers _retrieveUsers;

    [UsedImplicitly]
    public VoteRegister()
    { }
    
    public VoteRegister(IVoteRepository voteRepository, RetrieveUsers retrieveUsers)
    {
        _voteRepository = voteRepository;
        _retrieveUsers = retrieveUsers;
    }

    public virtual void Register(VoteDto voteDto)
    {
        var user = _retrieveUsers.GetUserBy(voteDto.UserId);
        var selectedUser = _retrieveUsers.GetUserBy(voteDto.SelectedUserId);
        var vote = Vote.Create(user, selectedUser);
        _voteRepository.Save(vote);
    }

    public virtual bool UserAlreadyVoted(string userId)
    {
        return _voteRepository.Exists(vote => vote.User.Id == userId);
    }
}