﻿using JetBrains.Annotations;

public class RetrieveVotes
{
    private readonly IVoteRepository _voteRepository;

    [UsedImplicitly]
    public RetrieveVotes()
    { }
    
    public RetrieveVotes(IVoteRepository voteRepository)
    {
        _voteRepository = voteRepository;
    }
    
    public virtual Vote[] GetVotes()
    {
        return _voteRepository.GetVotes();
    }
}