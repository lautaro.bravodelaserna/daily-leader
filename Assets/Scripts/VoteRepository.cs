﻿using System;
using System.Collections.Generic;
using System.Linq;

public class VoteRepository : IVoteRepository
{
    private readonly List<Vote> _votes = new List<Vote>();

    public void Save(Vote vote)
    {
        _votes.Add(vote);
    }

    public Vote[] GetVotes()
    {
        return _votes.ToArray();
    }

    public bool Exists(Func<Vote, bool> predicate)
    {
        return _votes.Any(predicate);
    }
}