﻿public class VoteDto
{
    public readonly string UserId;
    public readonly string SelectedUserId;

    public VoteDto(string userId, string selectedUserId)
    {
        UserId = userId;
        SelectedUserId = selectedUserId;
    }
}