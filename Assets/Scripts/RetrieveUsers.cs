﻿using System.Linq;
using JetBrains.Annotations;

public class RetrieveUsers
{
    private readonly IUserRepository _userRepository;

    [UsedImplicitly]
    public RetrieveUsers()
    { }
    
    public RetrieveUsers(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public virtual User GetUserBy(string userId)
    {
        return _userRepository.GetUserById(userId);
    }

    public virtual User[] GetOrderByScoreUsers()
    {
        return _userRepository.GetUsers()
            .OrderByDescending(user => user.Score)
            .ToArray();
    }
}