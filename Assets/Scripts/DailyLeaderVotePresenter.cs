﻿public class DailyLeaderVotePresenter
{
    private readonly IDailyLeaderVoteView _view;
    private readonly VoteRegister _voteRegister;
    private readonly RetrieveVotes _retrieveVotes;
    private readonly VoteStateHandler _voteStateHandler;
    private readonly IRetrieveOnlineUsers _retrieveOnlineUsers;
    private readonly RetrieveUsers _retrieveUsers;
    private readonly UpdateUserStats _updateUserStats;

    public DailyLeaderVotePresenter(IDailyLeaderVoteView view,
        VoteRegister voteRegister,
        RetrieveVotes retrieveVotes,
        VoteStateHandler voteStateHandler,
        IRetrieveOnlineUsers retrieveOnlineUsers,
        RetrieveUsers retrieveUsers, UpdateUserStats updateUserStats)
    {
        _view = view;
        _voteRegister = voteRegister;
        _retrieveVotes = retrieveVotes;
        _voteStateHandler = voteStateHandler;
        _retrieveOnlineUsers = retrieveOnlineUsers;
        _retrieveUsers = retrieveUsers;
        _updateUserStats = updateUserStats;
    }

    public void StartGame()
    {
        var users = _retrieveOnlineUsers.Get();
        _view.ShowUsers(users);
    }

    public void RegisterVote(VoteDto voteDto)
    {
        if (_voteStateHandler.IsClose() || _voteRegister.UserAlreadyVoted(voteDto.UserId))
            return;
        
        _voteRegister.Register(voteDto);
        _view.ShowConfirmationOfVoteFor(voteDto.UserId);
    }

    public void CloseVotes()
    {
        _voteStateHandler.CloseVotes();
        var votes = _retrieveVotes.GetVotes();
        _view.ShowVotes(votes);
    }

    public void WinnerSelected(string userId)
    {
        var selectedUser = _retrieveUsers.GetUserBy(userId);
        var votes = _retrieveVotes.GetVotes();
        _view.ShowWinner(selectedUser);
        _updateUserStats.FilterAndUpdateStatsWithCorrectVotes(votes, selectedUser);
        _updateUserStats.UpdateWinnerStatsFor(userId);
        var users = _retrieveUsers.GetOrderByScoreUsers();
        _view.ShowLeaderboard(users);
    }
}