﻿public class VoteStateHandler
{
    private bool _isClose;
    
    public virtual bool IsClose()
    {
        return _isClose;
    }
    
    public void CloseVotes()
    {
        _isClose = true;
    }
}