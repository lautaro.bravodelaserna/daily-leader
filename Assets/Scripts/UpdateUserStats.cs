﻿using System.Linq;
using JetBrains.Annotations;

public class UpdateUserStats
{
    private readonly IUserRepository _userRepository;

    [UsedImplicitly]
    public UpdateUserStats()
    { }
    
    public UpdateUserStats(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }
    
    public virtual void FilterAndUpdateStatsWithCorrectVotes(Vote[] votes, User userSelected)
    {
        votes.Where(vote => vote.SelectedUser.Is(userSelected.Id))
            .Select(vote => vote.User)
            .Select(user => user.IncreaseScore())
            .ToList()
            .ForEach(user => _userRepository.Save(user));
    }

    public virtual void UpdateWinnerStatsFor(string userId)
    {
        var user = _userRepository.GetUserById(userId);
        user.IncreaseWins();
        _userRepository.Save(user);
    }
}