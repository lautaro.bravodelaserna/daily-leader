﻿public class Vote
{
    public User User;
    public User SelectedUser;

    public Vote(User user, User selectedUser)
    {
        User = user;
        SelectedUser = selectedUser;
    }

    public static Vote Create(User user, User selectedUser)
    {
        return new Vote(user, selectedUser);
    }
}