﻿using System;
using NSubstitute;
using NUnit.Framework;

namespace Editor
{
    public class VoteRegisterTest
    {  
        private const string FIRST_USER_ID = "Pepito";
        private const string SECOND_USER_ID = "Jorge";
        private const string FOURTH_USER_ID = "Luz";

        private IVoteRepository _voteRepository;
        private RetrieveUsers _retrieveUsers;
        private VoteRegister _voteRegister;
        
        [SetUp]
        public void SetUp()
        {
            _retrieveUsers = Substitute.For<RetrieveUsers>();
            _voteRepository = Substitute.For<IVoteRepository>();
            _voteRegister = new VoteRegister(_voteRepository, _retrieveUsers);
        }
    
        [Test]
        public void register_when_request()
        {
            var vote = new VoteDto(FIRST_USER_ID, SECOND_USER_ID);
            var user = new User(FIRST_USER_ID);
            var selectedUser = new User(SECOND_USER_ID);
            
            _retrieveUsers.GetUserBy(FIRST_USER_ID).Returns(user);
            _retrieveUsers.GetUserBy(SECOND_USER_ID).Returns(selectedUser);
            _voteRegister.Register(vote);
            _voteRepository.Received(1).Save(Arg.Is<Vote>(v => v.User == user && v.SelectedUser == selectedUser));
        }

        [Test]
        public void user_already_vote_when_request()
        {
            _voteRepository.Exists(Arg.Any<Func<Vote,bool>>()).Returns(true);
            var exists = _voteRegister.UserAlreadyVoted(FIRST_USER_ID);
            Assert.IsTrue(exists, "Then user already voted");
            _voteRepository.Received(1).Exists(Arg.Any<Func<Vote, bool>>());
        }
    }
}