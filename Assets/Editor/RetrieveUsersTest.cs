﻿using System;
using NSubstitute;
using NUnit.Framework;

namespace Editor
{
    public class RetrieveUsersTest
    {
        private const string FIRST_USER_ID = "Pepito";
        private const string SECOND_USER_ID = "Jorge";
        private const string THIRD_USER_ID = "Luz";
        
        private const int FIRST_SCORE = 100;
        private const int SECOND_SCORE = 300;
        private const int THIRD_SCORE = 5000;
        
        private IUserRepository _userRepository;
        private RetrieveUsers _retrieveUsers;
        
        [SetUp]
        public void SetUp()
        {
            _userRepository = Substitute.For<IUserRepository>();
            _retrieveUsers = new RetrieveUsers(_userRepository);
        }
    
        [Test]
        public void get_user_by_id()
        {
            var expectedUser = new User(FIRST_USER_ID);
            _userRepository.GetUserById(FIRST_USER_ID).Returns(expectedUser);
            var user = _retrieveUsers.GetUserBy(FIRST_USER_ID);
            Assert.AreEqual(expectedUser, user);
        }

        [Test]
        public void get_users_order_by_score()
        {
            var expectedUsers = new[]
            {
                new User(FIRST_USER_ID, FIRST_SCORE),
                new User(SECOND_USER_ID, SECOND_SCORE),
                new User(THIRD_USER_ID, THIRD_SCORE),
            };
            _userRepository.GetUsers().Returns(expectedUsers);
            var users = _retrieveUsers.GetOrderByScoreUsers();
            Assert.AreEqual(THIRD_SCORE, users[0].Score);
            Assert.AreEqual(SECOND_SCORE, users[1].Score);
            Assert.AreEqual(FIRST_SCORE, users[2].Score);
        }
    }
}