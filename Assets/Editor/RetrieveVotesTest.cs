﻿using NSubstitute;
using NUnit.Framework;

namespace Editor
{
    public class RetrieveVotesTest
    {
        private const string FIRST_USER_ID = "Pepito";
        private const string SECOND_USER_ID = "Jorge";
        private const string THIRD_USER_ID = "Maca";
        private const string FOURTH_USER_ID = "Luz";

        private IVoteRepository _voteRepository;
        private RetrieveVotes _retrieveVotes;
        
        [SetUp]
        public void SetUp()
        {
            _voteRepository = Substitute.For<IVoteRepository>();
            _retrieveVotes = new RetrieveVotes(_voteRepository);
        }
    
        [Test]
        public void retrieve_votes_when_request()
        {
            var expectedVotes = new[]
            {
                new Vote(new User(FIRST_USER_ID), new User(SECOND_USER_ID)),
                new Vote(new User(THIRD_USER_ID), new User(FOURTH_USER_ID)),
            };
            _voteRepository.GetVotes().Returns(expectedVotes);
            var votes = _retrieveVotes.GetVotes();
            Assert.AreEqual(expectedVotes, votes);
        }   
    }
}