﻿using NSubstitute;
using NUnit.Framework;

namespace Editor
{
    public class UpdateUserStatsTest
    {
        private const string FIRST_ID = "Pepito";
        private const string SECOND_ID = "Diego";
        private const string THIRD_ID = "Jesu";
        private const string FOURTH_ID = "Jorge";

        private IUserRepository _userRepository;
        private UpdateUserStats _updateUserStats;
        
        [SetUp]
        public void SetUp()
        {
            _userRepository = Substitute.For<IUserRepository>();   
            _updateUserStats = new UpdateUserStats(_userRepository);
        }

        [Test]
        public void update_with_votes()
        {
            var otherUser = new User(FOURTH_ID);
            var winnerUser = new User(SECOND_ID);

            var votes = new[]
            {
                new Vote(new User(FIRST_ID),otherUser),
                new Vote(new User(SECOND_ID),winnerUser),
                new Vote(new User(THIRD_ID),otherUser),
                new Vote(new User(FOURTH_ID, 100),winnerUser),
            };
            _updateUserStats.FilterAndUpdateStatsWithCorrectVotes(votes, winnerUser);
            _userRepository.Received(0).Save(Arg.Is<User>(user => user.Score == 100 && user.Id == FIRST_ID));
            _userRepository.Received(1).Save(Arg.Is<User>(user => user.Score == 100 && user.Id == SECOND_ID));
            _userRepository.Received(0).Save(Arg.Is<User>(user => user.Score == 100 && user.Id == THIRD_ID));
            _userRepository.Received(1).Save(Arg.Is<User>(user => user.Score == 200 && user.Id == FOURTH_ID));
        }
    
        [Test]
        public void increase_winner_stats()
        {
            var user = new User(FIRST_ID, 0 , 1);
            _userRepository.GetUserById(FIRST_ID).Returns(user);
            _updateUserStats.UpdateWinnerStatsFor(FIRST_ID);
            _userRepository.Received(1).Save(Arg.Is<User>(u => u.Wins == 2));
        }   
    }
}