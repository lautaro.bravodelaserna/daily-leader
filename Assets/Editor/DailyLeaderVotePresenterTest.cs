﻿using NSubstitute;
using NUnit.Framework;

namespace Editor
{
    public class DailyLeaderVotePresenterTest
    {
        private const string FIRST_USER_ID = "Pepito";
        private const string SECOND_USER_ID = "Jorge";
        private const string THIRD_USER_ID = "Maca";
        private const string FOURTH_USER_ID = "Luz";

        private VoteRegister _voteRegister;
        private RetrieveVotes _retrieveVotes;
        private VoteStateHandler _voteStateHandler;
        private IRetrieveOnlineUsers _retrieveOnlineUsers;
        private RetrieveUsers _retrieveUsers;
        private UpdateUserStats _updateUserStats;
        private IDailyLeaderVoteView _view;
        private DailyLeaderVotePresenter _presenter;

        [SetUp]
        public void SetUp()
        {
            _voteStateHandler = new VoteStateHandler();
            _voteRegister = Substitute.For<VoteRegister>();
            _retrieveVotes = Substitute.For<RetrieveVotes>();
            _retrieveOnlineUsers = Substitute.For<IRetrieveOnlineUsers>();
            _retrieveUsers = Substitute.For<RetrieveUsers>();
            _updateUserStats = Substitute.For<UpdateUserStats>();
            _view = Substitute.For<IDailyLeaderVoteView>();
            _presenter = new DailyLeaderVotePresenter(_view, _voteRegister, _retrieveVotes, _voteStateHandler, _retrieveOnlineUsers, _retrieveUsers, _updateUserStats);
        }

        [Test]
        public void start_votes_when_start_game()
        {
            var users = new[]
            {
                new User(FIRST_USER_ID),
                new User(SECOND_USER_ID),
            };
            _retrieveOnlineUsers.Get().Returns(users);
            _presenter.StartGame();
            _view.Received(1).ShowUsers(users);
        }

        [Test]
        public void register_my_vote_when_request()
        {
            VoteDto voteDto = new VoteDto(FIRST_USER_ID, SECOND_USER_ID);
            _presenter.RegisterVote(voteDto);
            _voteRegister.Received(1).Register(voteDto);
            _view.Received(1).ShowConfirmationOfVoteFor(voteDto.UserId);
        }

        [Test]
        public void prevent_vote_if_i_already_voted()
        {
            VoteDto voteDto = new VoteDto(FIRST_USER_ID, SECOND_USER_ID);
            _presenter.RegisterVote(voteDto);
            _voteRegister.UserAlreadyVoted(FIRST_USER_ID).Returns(true);
            _presenter.RegisterVote(voteDto);
            _voteRegister.Received(1).Register(voteDto);
        }

        [Test]
        public void close_votes_and_show_votes_when_request()
        {
            Vote[] votes = new[]
            {
                new Vote(new User(FIRST_USER_ID), new User(SECOND_USER_ID)),
                new Vote(new User(THIRD_USER_ID), new User(FOURTH_USER_ID)),
            };
            _retrieveVotes.GetVotes().Returns(votes);
            _presenter.CloseVotes();
            _view.Received(1).ShowVotes(votes);
        }

        [Test]
        public void do_not_accept_new_votes_when_close_votes()
        {
            VoteDto voteDto = new VoteDto(FIRST_USER_ID, SECOND_USER_ID);

            _presenter.CloseVotes();
            _presenter.RegisterVote(voteDto);
            _voteRegister.Received(0).Register(voteDto);
        }

        [Test]
        public void show_winner_when_winner_selected()
        {
            var user = new User(FIRST_USER_ID);
            _retrieveUsers.GetUserBy(FIRST_USER_ID).Returns(user);
            _presenter.WinnerSelected(FIRST_USER_ID);
            _view.Received(1).ShowWinner(user);
        }

        [Test]
        public void update_user_stats_when_winner_selected()
        {
            _presenter.WinnerSelected(FIRST_USER_ID); 
            _updateUserStats.Received(1).FilterAndUpdateStatsWithCorrectVotes(Arg.Any<Vote[]>(), Arg.Any<User>());
        }

        [Test]
        public void show_user_stats_when_winner_selected()
        {
            var users = new[]
            {
                new User(FIRST_USER_ID),
                new User(SECOND_USER_ID),
            };
            _retrieveUsers.GetOrderByScoreUsers().Returns(users);
            _presenter.WinnerSelected(FIRST_USER_ID);
            _view.Received(1).ShowLeaderboard(users);
        }

        [Test]
        public void update_user_stats_when_was_selected()
        {
            _presenter.WinnerSelected(FIRST_USER_ID);
            _updateUserStats.Received(1).UpdateWinnerStatsFor(FIRST_USER_ID);
        }
    }
}